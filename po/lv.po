# translation of lv.po to Latvian
# Latvian translation for Gnome Desktop.
# Copyright © 2006 Gnome i18n Project for Latvian.
# Copyright (C) 2000, 2006, 2007, 2009 Free Software Foundation, Inc.
#
# P�eris Krij�is <peterisk@apollo.lv>, 2000.
# Artis Trops <hornet@navigators.lv>, 2000.
# Raivis Dejus <orvils@gmail.com>, 2006, 2007, 2009.
# Anita Reitere <nitalynx@gmail.com>, 2010.
# Rudolfs <rudolfs.mazurs@gmail.com>, 2011, 2012, 2013, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: lv\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues\n"
"POT-Creation-Date: 2019-07-15 00:21+0000\n"
"PO-Revision-Date: 2019-08-24 17:35+0200\n"
"Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>\n"
"Language-Team: Latvian <lata-l10n@googlegroups.com>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 :"
" 2);\n"
"X-Generator: Lokalize 2.0\n"

#: libgnome-desktop/gnome-languages.c:717
msgid "Unspecified"
msgstr "Nenorādīts"

#. TRANSLATORS: Used to distinguish the labels representing the gez_ER
#. and gez_ET locales from gez_ER@abegede respective gez_ET@abegede. The
#. difference is related to collation.
#: libgnome-desktop/gnome-languages.c:1377
msgid "Abegede"
msgstr "Abegede"

#: libgnome-desktop/gnome-languages.c:1378
msgid "Cyrillic"
msgstr "Kirilica"

#: libgnome-desktop/gnome-languages.c:1379
msgid "Devanagari"
msgstr "Dēvanāgarī"

#. TRANSLATORS: Used to distinguish the label representing the tt_RU
#. locale from tt_RU@iqtelif. It's a special alphabet for Tatar.
#: libgnome-desktop/gnome-languages.c:1382
msgid "IQTElif"
msgstr "IQTElif"

#. TRANSLATORS: The alphabet/script, not the language.
#: libgnome-desktop/gnome-languages.c:1384
msgid "Latin"
msgstr "Latīņu"

#. TRANSLATORS: "Saho" is a variant of the Afar language. Used to
#. distinguish the label representing the aa_ER locale from aa_ER@saaho.
#: libgnome-desktop/gnome-languages.c:1387
msgid "Saho"
msgstr "Saho"

#. TRANSLATORS: "Valencia" is a dialect of the Catalan language spoken
#. in Valencia. Used to distinguish the label representing the ca_ES
#. locale from ca_ES@valencia.
#: libgnome-desktop/gnome-languages.c:1391
msgid "Valencia"
msgstr "Valencija"

#: libgnome-desktop/gnome-rr-config.c:759
#, c-format
msgid "CRTC %d cannot drive output %s"
msgstr "CRT %d kontrolieris nevar vadīt izeju %s"

#: libgnome-desktop/gnome-rr-config.c:766
#, c-format
msgid "output %s does not support mode %dx%d@%dHz"
msgstr "izeja %s neatbalsta režīmu %dx%d@%dHz"

#: libgnome-desktop/gnome-rr-config.c:777
#, c-format
msgid "CRTC %d does not support rotation=%d"
msgstr "CRT %d kontrolieris neatbalsta rotāciju=%d"

#: libgnome-desktop/gnome-rr-config.c:790
#, c-format
msgid ""
"output %s does not have the same parameters as another cloned output:\n"
"existing mode = %d, new mode = %d\n"
"existing coordinates = (%d, %d), new coordinates = (%d, %d)\n"
"existing rotation = %d, new rotation = %d"
msgstr ""
"izvadei %s nav tie paši parametri kā citai klonētajai izvadei:\n"
"esošais režīms = %d, jaunais režīms = %d\n"
"esošās koordinātas = (%d, %d), jaunās koordinātas = (%d, %d)\n"
"esošā rotācija = %d, jaunā rotācija = %d"

#: libgnome-desktop/gnome-rr-config.c:805
#, c-format
msgid "cannot clone to output %s"
msgstr "nevar klonēt izeju %s"

#: libgnome-desktop/gnome-rr-config.c:931
#, c-format
msgid "Trying modes for CRTC %d\n"
msgstr "Mēģina režīmus CRT %d kontrolierim\n"

#: libgnome-desktop/gnome-rr-config.c:955
#, c-format
msgid "CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)\n"
msgstr ""
"CRT %d kontrolieris — mēģina režīmu %dx%d@%dHz ar izeju pie %dx%d@%dHz (pass "
"%d)\n"

#: libgnome-desktop/gnome-rr-config.c:1002
#, c-format
msgid ""
"could not assign CRTCs to outputs:\n"
"%s"
msgstr ""
"nevarēja piešķirt CRT kontrolieri pie izejām:\n"
"%s"

#: libgnome-desktop/gnome-rr-config.c:1006
#, c-format
msgid ""
"none of the selected modes were compatible with the possible modes:\n"
"%s"
msgstr ""
"neviens no izvēlētajiem režīmiem nebija savietojams ar iespējamiem "
"režīmiem:\n"
"%s"

#. Translators: the "requested", "minimum", and
#. * "maximum" words here are not keywords; please
#. * translate them as usual.
#: libgnome-desktop/gnome-rr-config.c:1087
#, c-format
msgid ""
"required virtual size does not fit available size: requested=(%d, %d), "
"minimum=(%d, %d), maximum=(%d, %d)"
msgstr ""
"pieprasītais virtuālais izmērs neatbilst pieejamajam izmēram — "
"pieprasīts=(%d, %d), minimums=(%d, %d), maksimums=(%d, %d)"

#. Translators: This is the time format with full date
#. plus day used in 24-hour mode. Please keep the under-
#. score to separate the date from the time.
#: libgnome-desktop/gnome-wall-clock.c:332
msgid "%a %b %-e_%R:%S"
msgstr "%a %-e_ %b, %R.%S"

#: libgnome-desktop/gnome-wall-clock.c:333
msgid "%a %b %-e_%R"
msgstr "%a %-e_ %b, %R"

#. Translators: This is the time format with full date
#. used in 24-hour mode. Please keep the underscore to
#. separate the date from the time.
#: libgnome-desktop/gnome-wall-clock.c:338
msgid "%b %-e_%R:%S"
msgstr "%-e_ %b, %R.%S"

#: libgnome-desktop/gnome-wall-clock.c:339
msgid "%b %-e_%R"
msgstr "%-e_ %b, %R"

#. Translators: This is the time format with day used
#. in 24-hour mode.
#: libgnome-desktop/gnome-wall-clock.c:343
msgid "%a %R:%S"
msgstr "%a %R.%S"

#: libgnome-desktop/gnome-wall-clock.c:344
msgid "%a %R"
msgstr "%a %R"

#. Translators: This is the time format without date used
#. in 24-hour mode.
#: libgnome-desktop/gnome-wall-clock.c:348
msgid "%R:%S"
msgstr "%R:%S"

#: libgnome-desktop/gnome-wall-clock.c:349
msgid "%R"
msgstr "%R"

#. Translators: This is a time format with full date
#. plus day used for AM/PM. Please keep the under-
#. score to separate the date from the time.
#: libgnome-desktop/gnome-wall-clock.c:357
msgid "%a %b %-e_%l:%M:%S %p"
msgstr "%a %-e_ %b, %H.%M.%S"

#: libgnome-desktop/gnome-wall-clock.c:358
msgid "%a %b %-e_%l:%M %p"
msgstr "%a %-e_ %b, %H.%M"

#. Translators: This is a time format with full date
#. used for AM/PM. Please keep the underscore to
#. separate the date from the time.
#: libgnome-desktop/gnome-wall-clock.c:363
msgid "%b %-e_%l:%M:%S %p"
msgstr "%-e_ %b, %H.%M.%S"

#: libgnome-desktop/gnome-wall-clock.c:364
msgid "%b %-e_%l:%M %p"
msgstr "%-e_ %b, %H.%M"

#. Translators: This is a time format with day used
#. for AM/PM.
#: libgnome-desktop/gnome-wall-clock.c:368
msgid "%a %l:%M:%S %p"
msgstr "%H.%M.%S"

#: libgnome-desktop/gnome-wall-clock.c:369
msgid "%a %l:%M %p"
msgstr "%H.%M"

#. Translators: This is a time format without date used
#. for AM/PM.
#: libgnome-desktop/gnome-wall-clock.c:373
msgid "%l:%M:%S %p"
msgstr "%H.%M.%S"

#: libgnome-desktop/gnome-wall-clock.c:374
msgid "%l:%M %p"
msgstr "%H.%M"

#~ msgctxt "Monitor vendor"
#~ msgid "Unknown"
#~ msgstr "Nezināms"

#~ msgid "could not get the screen resources (CRTCs, outputs, modes)"
#~ msgstr "neizdevās iegūt ekrāna resursus (CRTC, izvadus, režīmus)"

#~ msgid "unhandled X error while getting the range of screen sizes"
#~ msgstr "neapstrādāta X kļūda, iegūstot ekrāna izmēru diapazonu"

#~ msgid "could not get the range of screen sizes"
#~ msgstr "neizdevās iegūt ekrāna izmēru diapazonu"

#~ msgid "RANDR extension is not present"
#~ msgstr "RANDR paplašinājums nav pieejams"

#~ msgid "could not get information about output %d"
#~ msgstr "neizdevās atrast informāciju par izvadi %d"

#~ msgid "Built-in Display"
#~ msgstr "Iebūvēts displejs"

#~ msgid "%s Display"
#~ msgstr "%s displejs"

#~| msgctxt "Monitor vendor"
#~| msgid "Unknown"
#~ msgid "Unknown Display"
#~ msgstr "Nezināms displejs"

#~ msgid ""
#~ "requested position/size for CRTC %d is outside the allowed limit: "
#~ "position=(%d, %d), size=(%d, %d), maximum=(%d, %d)"
#~ msgstr ""
#~ "pieprasītā CRTC %d pozīcija/izmērs ir ārpus pieļaujamās robežas — "
#~ "pozīcija=(%d, %d), izmērs=(%d, %d), maksimums=(%d, %d)"

#~ msgid "could not set the configuration for CRTC %d"
#~ msgstr "neizdevās iestatīt CRTC %d konfigurāciju"

#~ msgid "could not get information about CRTC %d"
#~ msgstr "neizdevās atrast informāciju par CRTC %d"

#~ msgid ""
#~ "none of the saved display configurations matched the active configuration"
#~ msgstr ""
#~ "neviena no saglabātajām ekrāna konfigurācijām neatbilst aktīvajai "
#~ "konfigurācijai"

#~| msgid "%a %b %e, %R:%S"
#~ msgid "%a %b %e, %R∶%S"
#~ msgstr "%a %e %b, %R.%S"

#~ msgid "%R∶%S"
#~ msgstr "%R.%S"

#~| msgid "%a %l:%M:%S %p"
#~ msgid "%a %l∶%M∶%S %p"
#~ msgstr "%a %l∶%M∶%S %p"

#~| msgid "%a %l:%M %p"
#~ msgid "%a %l∶%M %p"
#~ msgstr "%a %l.%M %p"

#~ msgid "%l∶%M∶%S %p"
#~ msgstr "%l∶%M∶%S %p"

#~ msgid "%l∶%M %p"
#~ msgstr "%l.%M %p"

#~ msgid "Cannot find a terminal, using xterm, even if it may not work"
#~ msgstr "Nevar atrast termināli; lieto xterm, lai gan tas varētu nestrādāt"

#~ msgid "Laptop"
#~ msgstr "Klēpjdators"

#~ msgid "Mirrored Displays"
#~ msgstr "Dublēti displeji"

#~ msgid "About GNOME"
#~ msgstr "Par GNOME"

#~ msgid "Learn more about GNOME"
#~ msgstr "Uzziniet vairāk par GNOME"

#~ msgid "News"
#~ msgstr "Ziņas"

#~ msgid "GNOME Library"
#~ msgstr "GNOME bibliotēka"

#~ msgid "Friends of GNOME"
#~ msgstr "GNOME draugi"

#~ msgid "Contact"
#~ msgstr "Kontakti"

#~ msgid "The Mysterious GEGL"
#~ msgstr "Noslēpumainais GEGL"

#~ msgid "The Squeaky Rubber GNOME"
#~ msgstr "GNOME gumijas rūķītis"

#~ msgid "Wanda The GNOME Fish"
#~ msgstr "GNOME zivtiņa Vanda"

#~ msgid "_Open URL"
#~ msgstr "_Atvērt URL"

#~ msgid "_Copy URL"
#~ msgstr "_Kopēt URL"

#~ msgid "About the GNOME Desktop"
#~ msgstr "Par GNOME darbvidi"

#~ msgid "%(name)s: %(value)s"
#~ msgstr "%(name)s: %(value)s"

#~ msgid "Welcome to the GNOME Desktop"
#~ msgstr "Laipni lūgti GNOME vidē"

#~ msgid "Brought to you by:"
#~ msgstr "Veidotāji:"

#~ msgid "<b>%(name)s:</b> %(value)s"
#~ msgstr "<b>%(name)s:</b> %(value)s"

#~ msgid "Version"
#~ msgstr "Versija"

#~ msgid "Distributor"
#~ msgstr "Izplatītājs"

#~ msgid "Build Date"
#~ msgstr "Būvējuma datums"

#~ msgid "Display information on this GNOME version"
#~ msgstr "Informācija par šo GNOME versiju"

#~ msgid ""
#~ "GNOME also includes a complete development platform for applications "
#~ "programmers, allowing the creation of powerful and complex applications."
#~ msgstr ""
#~ "GNOME sastāvā ir arī pilnvērtīga programmatūras izstrādes platforma, kas "
#~ "ļauj veidot daudzfunkcionālas un jaudīgas programmas."

#~ msgid ""
#~ "GNOME includes most of what you see on your computer, including the file "
#~ "manager, web browser, menus, and many applications."
#~ msgstr ""
#~ "GNOME sevī iekļauj lielāko daļu no tā, ko jūs redzat uz datora, ieskaitot "
#~ "failu pārvaldnieku, tīkla pārlūku, izvēlnes un daudzas programmas."

#~ msgid ""
#~ "GNOME is a Free, usable, stable, accessible desktop environment for the "
#~ "Unix-like family of operating systems."
#~ msgstr ""
#~ "GNOME ir brīva, lietojama, stabila un visiem pieejama darba vide UNIX "
#~ "tipa operētājsistēmām."

#~ msgid ""
#~ "GNOME's focus on usability and accessibility, regular release cycle, and "
#~ "strong corporate backing make it unique among Free Software desktops."
#~ msgstr ""
#~ "GNOME raksturīgās iezīmes ir uzsvars uz lietojamību un pieejamību, "
#~ "regulārs laidienu cikls un spēcīgs korporatīvais atbalsts."

#~ msgid ""
#~ "GNOME's greatest strength is our strong community. Virtually anyone, with "
#~ "or without coding skills, can contribute to making GNOME better."
#~ msgstr ""
#~ "GNOME lielākais spēks slēpjas kopienā. Praktiski jebkurš, ar vai bez "
#~ "programmēšanas iemaņām, var līdzdarboties, lai padarītu GNOME labāku."

#~ msgid ""
#~ "Hundreds of people have contributed code to GNOME since it was started in "
#~ "1997; many more have contributed in other important ways, including "
#~ "translations, documentation, and quality assurance."
#~ msgstr ""
#~ "Simtiem programmētāju ir ieguldījuši savu darbu GNOME projektā kopš tā "
#~ "uzsākšanas 1997. gadā; vēl vairāk cilvēku ir palīdzējuši citos nozīmīgos "
#~ "veidos - tulkojot, rakstot dokumentāciju un nodrošinot kvalitātes "
#~ "pārbaudi."

#~ msgid "Error reading file '%s': %s"
#~ msgstr "Kļūda, lasot failu '%s': %s"

#~ msgid "Error rewinding file '%s': %s"
#~ msgstr "Kļūda, atgriežot failu '%s': %s"

#~ msgid "No name"
#~ msgstr "Bez nosaukuma"

#~ msgid "File '%s' is not a regular file or directory."
#~ msgstr "Fails '%s' nav parasts fails vai mape."

#~ msgid "Cannot find file '%s'"
#~ msgstr "Nevar atrast failu '%s'"

#~ msgid "No filename to save to"
#~ msgstr "Nav faila nosaukuma, kurā saglabāt"

#~ msgid "Starting %s"
#~ msgstr "Palaiž %s"

#~ msgid "No URL to launch"
#~ msgstr "Nav URL, ko palaist"

#~ msgid "Not a launchable item"
#~ msgstr "Nav palaižamas vienības"

#~ msgid "No command (Exec) to launch"
#~ msgstr "Nav komandas (Exec), ko palaist"

#~ msgid "Bad command (Exec) to launch"
#~ msgstr "Slikta komanda (Exec), ko palaist"

#~ msgid "Unknown encoding of: %s"
#~ msgstr "Nezināms kodējums: %s"

#~ msgid "Mirror Screens"
#~ msgstr "Spoguļekrāni"

#~ msgid "could not find a suitable configuration of screens"
#~ msgstr "neizdevās atrast piemērotu ekrānu konfigurāciju"
